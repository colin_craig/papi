from flask import Flask, jsonify, request
from api.models import Product, ProductSchema
from api import session, app

@app.route('/product/<int:id>', methods=['GET'])
def index(id):
    # Find one product by ID.
    one = session.query(Product).filter(Product.id == str(id)).one()

    result, error = ProductSchema().dump(one) 
    return jsonify(product = result)

@app.route('/product', methods=['POST'])
def create(self):
    # Create a Product.
    return

@app.route('/product/<int:id>', methods=['PATCH','PUT'])
def update(self, id):
    # Update a Product by ID.
    return

@app.route('/product/<int:id>', methods=['DELETE'])
def delete(self, id):
    # Delete a Product.
    return