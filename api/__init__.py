from flask import Flask
from flask.ext.mysql import MySQL
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import *
from sqlalchemy.orm import create_session
from sqlalchemy.ext.declarative import declarative_base


app = Flask(__name__)

DB_HOST = '192.168.33.20'
DB_PASS = 'IMVaTXrIRrir'
DB_USER = 'sg'
DB_DB   = 'marketplace'

dbConn = "mysql://" + DB_USER + ":" + DB_PASS +"@" + DB_HOST + "/" + DB_DB
app.config['SQLALCHEMY_DATABASE_URI'] = dbConn

db = SQLAlchemy(app)

#Create and engine and get the metadata
Base = declarative_base()
engine = create_engine(dbConn)
metadata = MetaData(bind=engine)

#Create a session to use the tables    
session = create_session(bind=engine)

# Uncomment to create the schema upon initialization.
# db.create_all()
# db.session.commit()

from api import views, models

if __name__ == '__main__':
    app.run()
