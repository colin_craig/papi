import datetime
from api import db, metadata, Base
#from sqlalchemy import *
from sqlalchemy.orm import relationship, backref
from sqlalchemy import ForeignKey, Table
from marshmallow import Schema, fields

class Product(Base):
    '''''
    Reflection of the sg_product table.
    '''''
    __table__ = Table('sg_product', metadata, autoload=True)
    Store = relationship('Store', backref='store_id')

class Store(Base):
    '''''
    Reflection of the sg_store table.
    '''''
    __table__ = Table('sg_store', metadata, autoload=True)

class StoreSchema(Schema):
    '''''
    Describes the data, and how we want it to appear in a json response.
    Add more fields as nesc.
    '''''
    # Make sure to use the 'only' or 'exclude' params
    # to avoid infinite recursion
    name = fields.String()
    email = fields.String()
    #class Meta:
    #    fields = ('name', 'email')

class ProductSchema(Schema):
    '''''
    Describes the data, and how we want it to appear in a json response.
    Add more fields as nesc.
    '''''
    Store = fields.Nested(StoreSchema)
    name = fields.String()



