Flask Product API Example
=========================

This is a jumping off point for a redesign of the Product Endpoint. Currently only a GET request is implemented using the marketplaces' Product/Store tables. This example doesn't even begin to touch the more complicated business logic of the product endpoint.
  
  
Installation  
------------  
  
  1. Install [pip](http://www.pip-installer.org/en/latest/installing.html)  
  2. Make a [virtualenv](http://virtualenvwrapper.readthedocs.org/en/latest/#introduction) for this project  
  3. Install the required dependencies: `pip install -r requirements.txt`  
  
Run the Product API:  
     
    python manage.py runserver  
  
Goto: [http://localhost:5000/product/<id>](http://localhost:5000/product/<id>)  

Why?
-----
The current Product Endpoint is bogged down with overhead, and we need to move towards a simplified/minimal overhead API implementation. 

How does it perform?
-----
  
$: ab -k -c 10 -n 200 localhost:5000/product/<id>  
  
Document Path:          /product/100000267  
Document Length:        142 bytes  
  
Concurrency Level:      10  
Time taken for tests:   1.717 seconds  
Complete requests:      200  
Failed requests:        0  
Write errors:           0  
Keep-Alive requests:    0  
Total transferred:      58000 bytes  
HTML transferred:       28400 bytes  
**Requests per second:    116.51 [#/sec] (mean)**  
Time per request:       85.830 [ms] (mean)  
**Time per request:       8.583 [ms] (mean, across all concurrent requests)**  
Transfer rate:          33.00 [Kbytes/sec] received  
  
Connection Times (ms)  
              min  mean[+/-sd] median   max  
Connect:        0    1   0.4      1       3  
Processing:    33   83   7.3     83     119  
Waiting:       33   83   7.3     83     119  
Total:         35   84   7.3     84     120  
  
Percentage of the requests served within a certain time (ms)  
  50%     84  
  66%     86  
  75%     87  
  80%     88  
  90%     90  
  95%     93  
  98%     98  
  99%    112  
 100%    120 (longest request)  
    
$: ab -k -c 10 -n 200 https://dev.sgmarketplace.com/api/v1/products/<id>?key=API_KEY  
  
Server Software:        nginx  
Server Hostname:        dev.sgmarketplace.com  
Server Port:            443  
SSL/TLS Protocol:       TLSv1/SSLv3,ECDHE-RSA-AES256-GCM-SHA384,1024,256  
  
Document Path:          /api/v1/products/56294b02bff8538e2b8b4567?key=48d0d53d84c1c805f193d341130ae168  
Document Length:        4535 bytes  
  
Concurrency Level:      10  
Time taken for tests:   236.333 seconds  
Complete requests:      200  
Failed requests:        0  
Write errors:           0  
Keep-Alive requests:    0  
Total transferred:      1013200 bytes  
HTML transferred:       907000 bytes  
**Requests per second:    0.85 [#/sec] (mean)**  
Time per request:       11816.664 [ms] (mean)  
**Time per request:       1181.666 [ms] (mean, across all concurrent requests)**  
Transfer rate:          4.19 [Kbytes/sec] received  
  
Connection Times (ms)   
              min  mean[+/-sd] median   max  
Connect:        3   10  13.5      6      95  
Processing:  4297 11627 2524.4  10697   15391  
Waiting:     4297 11626 2524.5  10697   15391  
Total:       4308 11637 2524.2  10702   15400  
  
Percentage of the requests served within a certain time (ms)  
  50%  10702  
  66%  13904  
  75%  14113  
  80%  14205  
  90%  14425  
  95%  14758  
  98%  15097  
  99%  15304  
 100%  15400 (longest request)  
  
* The Flask example was served via a single threaded development server. Existing API was served via Nginx. Additionally, the payload for the existing api is considerably larger.  
  
** After investigating, overhead (Time ) takes up about 90-95% of the existing API's request bandwidth.
   That's why I didn't bother fleshing out the Model classes, as they were not the bottleneck. However, if fully implemented, it would negatively impact the throughput of the prototype.  
  
Summary  
----------  
  
Requests/second - GET  
	Flask Prototype - **116.51**  
	Existing API    - **0.85**  
  
Memory[bytes]/request - GET  
    Flask Prototype - **<10000**   
	Existing API    - **85000000**